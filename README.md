KLIPPER > SKR 1.4 Turbo > TT Sapphire Pro

- BTT SKR 1.4 Turbo
- Fysetc mini LCD12864 Screen
- 4 X TMC2209
- Klipper 0.9.X
- Kiauh as Script to install Klipper + Fluidd | Mainsail
- 3 X 0.9 Angle motors (e3D Online) - High Torque [XYZ]
- 1 X 0.9 Angle motor (e3D Online) - Compact but Powerfull [E]
- TriangleLab V6 Clone: titanium heatbreak + cooper plated nozzle + 50W
- TriangleLab v1 BMG Clone: 600mm PTFE tube (bowden)
- Bed Suporte Kit + Aluminium 5mm + Silicone heated bed [all KIS3D]
- Linear Rails: Hiwin
- Bearings: Misumi